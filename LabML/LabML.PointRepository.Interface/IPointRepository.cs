﻿using LabML.CoreDomain.SharedObjects;
using System.Collections.Generic;

namespace LabML.PointRepository.Interface
{
    public interface IPointRepository
    {
        IEnumerable<Point> GetPoints();

        void AddPoints(IEnumerable<Point> points);
        void AddPoint(Point point);
    }
}