﻿using System;
using System.Collections.Concurrent;
using Newtonsoft.Json;

namespace LabML.Util.Serialization
{
    public abstract class ConcurrentBagConverterBase : JsonConverter
    {
        protected TConcurrentBag ReadJsonGeneric<TConcurrentBag, TItem>(JsonReader reader, Type collectionType,
            Type itemType, object existingValue, JsonSerializer serializer)
            where TConcurrentBag : ConcurrentBag<TItem>
        {
            if (reader.TokenType == JsonToken.Null)
            {
                return null;
            }
            if (reader.TokenType != JsonToken.StartArray)
            {
                throw new JsonSerializationException(string.Format("Expected {0}, encountered {1} at path {2}",
                    JsonToken.StartArray, reader.TokenType, reader.Path));
            }
            var collection = existingValue as TConcurrentBag ??
                             (TConcurrentBag)
                             serializer.ContractResolver.ResolveContract(collectionType).DefaultCreator();

            while (reader.Read())
            {
                switch (reader.TokenType)
                {
                    case JsonToken.Comment:
                        break;
                    case JsonToken.EndArray:
                        return collection;
                    default:
                        collection.Add((TItem) serializer.Deserialize(reader, itemType));
                        break;
                }
            }
            // Should not come here.
            throw new JsonSerializationException("Unclosed array at path: " + reader.Path);
        }

        protected void WriteJsonGeneric<TConcurrentBag, TItem>(JsonWriter writer, object value,
            JsonSerializer serializer)
            where TConcurrentBag : ConcurrentBag<TItem>
        {
            // Snapshot the bag as an array and serialize the array.
            var array = ((TConcurrentBag) value).ToArray();
            serializer.Serialize(writer, array);
        }
    }
}