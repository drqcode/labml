﻿using System;
using System.Collections.Concurrent;
using Newtonsoft.Json;

namespace LabML.Util.Serialization
{
    public class ConcurrentBagConverter<TItem> : ConcurrentBagConverterBase
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(ConcurrentBagConverter<TItem>).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            return ReadJsonGeneric<ConcurrentBag<TItem>, TItem>(reader, objectType, typeof(TItem), existingValue,
                serializer);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            WriteJsonGeneric<ConcurrentBag<TItem>, TItem>(writer, value, serializer);
        }
    }
}