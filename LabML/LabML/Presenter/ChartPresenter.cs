﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using LabML.BusinessLogic;
using LabML.Infrastructure;
using LabML.Model;
using LabML.View;

namespace LabML.Presenter
{
    public class ChartPresenter
    {
        private Chart _chart;
        private IChartView _chartView;


        public ChartPresenter(IChartView chartView)
        {

            _chartView = chartView;
            _chartView.InitView();

            LoadChartModel();
            SetViewPropertiesFromModel();

            _chartView.UpdateView(_chart);
        }

        private void SetViewPropertiesFromModel()
        {
            _chartView.MinXValue = _chart.XMin;
            _chartView.MaxXValue = _chart.XMax;
            _chartView.MinYValue = _chart.YMin;
            _chartView.MaxYValue = _chart.YMax;
        }

        private void LoadChartModel()
        {
            try
            {
                _chart = ChartJsonSerializer.LoadChart();
            }
            catch (FileNotFoundException)
            {
                _chart = new Chart(-100, 100, -100, 100);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        internal void GenerateNewDataPoints()
        {
            Task.Factory.StartNew(() => PointGenerator.GeneratePoints(new PointGeneratorParams(-200, 300, 40, 20,1000), _chart));
            Task.Factory.StartNew(() => PointGenerator.GeneratePoints(new PointGeneratorParams(150, 150, 10, 5,1000), _chart));
            Task.Factory.StartNew(
                () => PointGenerator.GeneratePoints(new PointGeneratorParams(-150, -150, 10, 30,1000), _chart));
        }


    }
}