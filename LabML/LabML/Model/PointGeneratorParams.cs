﻿namespace LabML.Model
{
    public struct PointGeneratorParams
    {
        private double _centerPointX;
        private double _centerPointY;
        private double _xDispersion;
        private double _yDispersion;

        private int _numberOfPoints;

        public PointGeneratorParams(double x,double y,double xDispersion,double yDispersion, int numberOfPoints)
        {
            _numberOfPoints = numberOfPoints;
            _centerPointX = x;
            _centerPointY = y;
            _xDispersion = xDispersion;
            _yDispersion = yDispersion;
        }

        public double CenterPointX
        {
            get
            {
                return _centerPointX;
            }

            set
            {
                _centerPointX = value;
            }
        }

        public double CenterPointY
        {
            get
            {
                return _centerPointY;
            }

            set
            {
                _centerPointY = value;
            }
        }

        public double XDispersion
        {
            get
            {
                return _xDispersion;
            }

            set
            {
                _xDispersion = value;
            }
        }

        public double YDispersion
        {
            get
            {
                return _yDispersion;
            }

            set
            {
                _yDispersion = value;
            }
        }

        public int NumberOfPoints
        {
            get { return _numberOfPoints; }
            set { _numberOfPoints = value; }
        }
    }
}