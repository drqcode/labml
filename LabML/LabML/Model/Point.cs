﻿using System;

namespace LabML.Model
{
    [Serializable]
    public struct Point
    {
        private double _x;
        private double _y;
        private int _tag;


        public Point(double x, double y, int tag)
        {
            _x = x;
            _y = y;
            _tag = tag;
        }


        public double X
        {
            get { return _x; }

            set { _x = value; }
        }

        public double Y
        {
            get { return _y; }

            set { _y = value; }
        }


        public int Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        public override string ToString()
        {
            return _x + " " + _y + " " + _tag;
        }
    }
}