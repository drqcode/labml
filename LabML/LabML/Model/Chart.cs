﻿using System;
using System.Collections.Concurrent;
using LabML.Util.Serialization;
using Newtonsoft.Json;

namespace LabML.Model
{
    public delegate void OnChangedHandler(Chart changedChart);

    public class Chart
    {
        private ConcurrentBag<Point> _points;
        private double _xMax;

        private double _xMin;
        private double _yMax;
        private double _yMin;

        public Chart(double xMin, double xMax, double yMin, double yMax)
        {
            _xMin = xMin;
            _xMax = xMax;
            _yMin = yMin;
            _yMax = yMax;

            _points = new ConcurrentBag<Point>();
        }

        public double XMin
        {
            get { return _xMin; }

            set { _xMin = value; }
        }

        public double YMin
        {
            get { return _yMin; }

            set { _yMin = value; }
        }

        public double XMax
        {
            get { return _xMax; }

            set { _xMax = value; }
        }

        public double YMax
        {
            get { return _yMax; }

            set { _yMax = value; }
        }

        [JsonConverter(typeof(ConcurrentBagConverter<Point>))]
        [JsonProperty]
        public ConcurrentBag<Point> Points
        {
            get { return _points; }

            private set { _points = value; }
        }

        public event OnChangedHandler Changed;

        public void AddPoint(Point point)
        {
            if (point.X < _xMin || point.X > _xMax || point.Y < _yMin || point.Y > _yMax)
            {
                throw new Exception("Point not in domain!");
            }
            _points.Add(point);
        }
    }
}