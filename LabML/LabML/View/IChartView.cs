﻿using LabML.Model;

namespace LabML.View
{
    public interface IChartView
    {
        double MaxXValue { set; }
        double MinXValue { set; }
        double MinYValue { set; }
        double MaxYValue { set; }

        

        void InitView();
        void UpdateView(Chart _chart);
    }
}