﻿namespace LabML
{
    partial class PointsViewerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pointChartPictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pointChartPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // pointChartPictureBox
            // 
            this.pointChartPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pointChartPictureBox.Location = new System.Drawing.Point(0, 0);
            this.pointChartPictureBox.Name = "pointChartPictureBox";
            this.pointChartPictureBox.Size = new System.Drawing.Size(1036, 587);
            this.pointChartPictureBox.TabIndex = 0;
            this.pointChartPictureBox.TabStop = false;
            // 
            // PointsViewerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1036, 587);
            this.Controls.Add(this.pointChartPictureBox);
            this.Name = "PointsViewerForm";
            this.Text = "Points Viewer";
            ((System.ComponentModel.ISupportInitialize)(this.pointChartPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pointChartPictureBox;
    }
}