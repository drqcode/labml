﻿using System;
using System.IO;
using System.Reflection;
using LabML.Model;
using LabML.Properties;
using Newtonsoft.Json;

namespace LabML.Infrastructure
{
    public static class ChartJsonSerializer
    {
        private static string userDir =
            Path.GetDirectoryName(
                Uri.UnescapeDataString(new UriBuilder(Assembly.GetExecutingAssembly().CodeBase).Path)) + "\\" +
            Resources.UserDirectory;

        private static string chartFilePath = userDir + "\\" + Resources.ChartFile;

        public static void SaveChart(Chart chart)
        {
            if (!Directory.Exists(userDir))
            {
                Directory.CreateDirectory(userDir);
            }
            using (var stream = new StreamWriter(File.Open(chartFilePath, FileMode.OpenOrCreate)))
            {
                JsonSerializer.Create(new JsonSerializerSettings {Formatting = Formatting.Indented})
                    .Serialize(stream, chart);
            }
        }

        public static Chart LoadChart()
        {
            Chart persistedChart;

            if (!File.Exists(chartFilePath))
            {
                throw new FileNotFoundException(chartFilePath+" not found!");
            }

            using (var stream = new StreamReader(File.Open(chartFilePath, FileMode.Open)))
            {
                persistedChart = (Chart) JsonSerializer.Create().Deserialize(stream, typeof(Chart));
            }
            return persistedChart;
        }
    }
}