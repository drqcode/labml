﻿namespace LabML.AiFramework
{
    public class AiClassifier<TClusteringElement, TResult> where TResult:IClusterCollection, TClusteringElement:
    {
        private IClusteringAlgorithm _algorithm;
	    private TResult _clusters;


        protected AiClassifier(IAiClassifierBuilder builder)
        {
	        _algorithm = ClassificationAlgorithmFactory.GetAlgorithm(builder.Algorithm);
        }

	    protected IClusteringAlgorithm Algorithm
	    {
		    get { return _algorithm; }
		    set { _algorithm = value; }
	    }

	    public TResult Clusters
	    {
		    get { return _clusters; }
	    }
    }
}