﻿namespace LabML.AiFramework
{
	public interface IClusteringElement
	{
		ICluster Cluster { get; set; }
	}
}