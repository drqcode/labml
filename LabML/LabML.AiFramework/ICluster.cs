﻿using System.Collections.Generic;

namespace LabML.AiFramework
{
	public interface ICluster
	{
		IClusteringElement Centroid { get; set; }
		IEnumerable<IClusteringElement> Elements { get; set; }

		void AddElement(IClusteringElement element);
	}
}