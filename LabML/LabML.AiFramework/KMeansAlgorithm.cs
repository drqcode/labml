﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Threading.Tasks;

namespace LabML.AiFramework
{
	public class KMeansAlgorithm
	{
		private IClusterCollection _clusters;
		private int _numberOfClusters;
		private ISimiliarityFinder _similiarity;

	    private double _convergenceThreshold=0;

		public IClusterCollection Clusters => _clusters;

		public event EventHandler Step;

	    public IClusterCollection Solve(ClusteringElement[] unClusteredData)
	    {
	        _clusters = ClusterCollectionFactory.CreateClusterCollection();

	        _clusters.RandomizeCentroids();

	        var converged = false;

	        while (!converged)
	        {
                var oldConvergence = _clusters.Convergence;

                foreach (var clusteringElement in unClusteredData)
	            {
	                _clusters.Decide(clusteringElement);
	            }

	            _clusters.ComputeNewCentroids();

	            var newConvergence = _clusters.Convergence;

		        converged = IsConverged(oldConvergence, newConvergence);
	        }

		    return _clusters;

	    }

		private bool IsConverged(double oldConvergence, double newConvergence)
		{
			return Math.Abs(newConvergence - oldConvergence) < _convergenceThreshold;
		}

		public void NStepsSolve(int numberOfSteps)
		{
			while (numberOfSteps-- > 0)
			{
				OneStepSolve();
				Step?.Invoke(this,new ClassificationResultEventArgs(_clusters));
			}
		}

		public void OneStepSolve()
		{
			foreach (var clusteringElement in _clusters.ClusteringElements)
			{
				double similiarity=double.MaxValue;
				foreach (var cluster in _clusters.Clusters)
				{
					var similiarityTmp = _similiarity.Find(clusteringElement,cluster.Centroid);
					if (similiarityTmp < similiarity)
					{
						similiarity = similiarityTmp;
						MoveElementIntoCluster(clusteringElement, cluster);
					}
				}
			}

			ChangeCentroidsCharacteristics();
		}

		private void ChangeCentroidsCharacteristics()
		{
			foreach (var cluster in _clusters.Clusters)
			{
				foreach (var element in cluster.Elements)
				{
					var props  = element.GetType().GetProperties();
					foreach (var propertyInfo in props)
					{
						foreach (var customAttribute in propertyInfo.GetCustomAttributes(true))
						{
							var numCharacteristic = customAttribute as NumericCharacteristicAttribute;
							if (numCharacteristic !=null)
							{
								
							}
						}
					}
				}
			}
		}

		private void MoveElementIntoCluster(IClusteringElement element, ICluster cluster)
		{
			element.Cluster = cluster;
			cluster.AddElement(element);
		}

	}
}