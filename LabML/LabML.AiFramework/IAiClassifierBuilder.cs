﻿namespace LabML.AiFramework
{
    public interface IAiClassifierBuilder
    {
	    ClusteringAlgorithmType Algorithm { get; set; }
    }
}