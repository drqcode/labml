﻿using System;

namespace LabML.AiFramework
{
	public class ClassificationResultEventArgs : EventArgs
	{
		private IClusterCollection _clusters;

		public IClusterCollection Clusters => _clusters;

		public ClassificationResultEventArgs(IClusterCollection clusters)
		{
			_clusters = clusters;
		}
	}
}