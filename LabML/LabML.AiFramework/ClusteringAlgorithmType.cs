﻿namespace LabML.AiFramework
{
	public enum ClusteringAlgorithmType
	{
		KMeans,
		Kohonen
	}
}