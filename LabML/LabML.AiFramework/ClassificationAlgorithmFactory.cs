﻿namespace LabML.AiFramework
{
	public static class ClassificationAlgorithmFactory
	{
		public static IClusteringAlgorithm GetAlgorithm(ClusteringAlgorithmType algorithmType)
		{
			switch (algorithmType)
			{
					case ClusteringAlgorithmType.KMeans: return new KMeansAlgorithm();
					case ClusteringAlgorithmType.Kohonen: return new KohonenAlgorithm();
				case default: return null;
			}
		}
	}
}