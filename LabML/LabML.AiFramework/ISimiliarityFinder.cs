﻿using System;

namespace LabML.AiFramework
{
	public interface ISimiliarityFinder
	{
		double Find(IClusteringElement element1, IClusteringElement element2);
	}
}