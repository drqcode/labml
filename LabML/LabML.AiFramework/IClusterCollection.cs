﻿using System.Collections.Generic;

namespace LabML.AiFramework
{
    public interface IClusterCollection
    {
	    IEnumerable<ICluster> Clusters { get; }
		IEnumerable<IClusteringElement> ClusteringElements { get; set; }
        object Centroids { get; set; }
        double Convergence { get; set; }

        void RandomizeCentroids();
        void Decide(ClusteringElement clusteringElement);
        void ComputeNewCentroids();
    }
}