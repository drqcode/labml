﻿using System;
using System.Threading.Tasks;

namespace LabML.AiFramework
{
    public interface IClusteringAlgorithm
    {
		IClusterCollection Clusters { get; }

        void Solve();

	    event EventHandler Step;
    }
}