﻿using System;
using System.Collections.Generic;

namespace LabML.CoreDomain.DomainObjects
{
    public class PointChart
    {
        private IList<Point> _points;

        public PointChart(IEnumerable<Point> points, PointDomain pointsDomain)
        {
            _points = new List<Point>(points);
            PointsDomain = pointsDomain;
        }

        public IEnumerable<Point> Points { get; }
        public PointDomain PointsDomain { get; private set; }

        public void AddNewPoint(double x, double y)
        {
            var point = new Point(x,y,PointsDomain);
            _points.Add(point);
            
        }
    }
}