﻿namespace LabML.CoreDomain.DomainObjects
{
    public struct PointDomain
    {
        public double XMin => _xMin;
        public double XMax => _xMax;
        public double YMin => _yMin;
        public double YMax => _yMax;

        private double _xMin;
        private double _xMax;
        private double _yMin;
        private double _yMax;

        public PointDomain(double xMin, double xMax, double yMin, double yMax)
        {
            _xMin = xMin;
            _xMax = xMax;
            _yMin = yMin;
            _yMax = yMax;
        }

        
    }
}