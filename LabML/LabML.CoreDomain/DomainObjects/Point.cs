﻿using System;
using LabML.AiFramework;

namespace LabML.CoreDomain.DomainObjects
{
    public class Point
    {
        private double _x;
        private double _y;

        private PointDomain _pointDomain;

        public Point(double x, double y, PointDomain domain)
        {
            if(x<domain.XMin || x>domain.XMax) throw new Exception("Point not in domain");
            if(y<domain.YMin || y>domain.YMax) throw new Exception("Point not in domain");
            _x = x;
            _y = y;
            _pointDomain = domain;
        }

        [NumericCharacteristic]
        public double X => _x;
        public double Y => _y;

    }
}