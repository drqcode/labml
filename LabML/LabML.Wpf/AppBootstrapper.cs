﻿using System;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;

namespace LabML.Wpf
{
    public class AppBootstrapper : BootstrapperBase
    {
        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            Application.Exit+=ApplicationOnExit;
        }

        private void ApplicationOnExit(object sender, ExitEventArgs exitEventArgs)
        {
            try
            {
                var cleanupTask = Task.Run(() =>
                {
                    var container = IoC.Container;
                    if (container != null)
                    {

                    }
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}