﻿using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Practices.Unity.ObjectBuilder;
using Microsoft.Practices.Unity;
using System;
using System.Linq;

namespace LabML.Wpf
{
    public static class IoC
    {
        public static IUnityContainer Container { get; private set; }

        [SuppressMessage("Microsoft.Naming", "CA1709:IdentifierShoudBeCasedCorrectly", MessageId = "Io")]
        public static void IoCisComing()
        {
            Container = new UnityContainer();
            RegisterTypes(Container);
        }

        private static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterTypes(
                AllClasses.FromLoadedAssemblies()
                    .Where(type => type.Namespace == "LabML.Wpf" && type.Namespace == "LabML.CoreDomain"),
                WithMappings.FromMatchingInterface,
                WithName.Default
            );
        }
    }
}