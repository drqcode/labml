﻿using System;
using System.Windows.Input;
using Caliburn.Micro;

namespace LabML.Wpf.ApplicationFeatures
{
    public abstract class ActionBase: PropertyChangedBase, ICommand
    {
        protected ActionBase()
        {
            
        }

        public bool CanExecute(object parameter)
        {
            throw new NotImplementedException();
        }

        public void Execute(object parameter)
        {
            throw new NotImplementedException();
        }

        public event EventHandler CanExecuteChanged;
    }
}