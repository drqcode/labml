﻿using Caliburn.Micro;

namespace LabML.Wpf.ApplicationFeatures.Alerts
{
    public static class Alert
    {
        public static IEventAggregator EventAggregator { get; set; }

        public static void OfError(string title, string message)
        {
            EventAggregator.PublishOnUIThread(new AlertMessage(title,message,"ERROR"));
        }
    }
}