﻿namespace LabML.Wpf.ApplicationFeatures.Alerts
{
    public class AlertMessage
    {
        public AlertModel Alert { get; private set; }

        public AlertMessage(string title, string message, string severity)
        {
            Alert=new AlertModel(title,message,severity);
        }
    }
}