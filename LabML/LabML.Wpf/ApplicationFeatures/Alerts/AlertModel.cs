﻿using System;
using System.Timers;
using Caliburn.Micro;

namespace LabML.Wpf.ApplicationFeatures.Alerts
{
    public class AlertModel : PropertyChangedBase, IDisposable
    {
        private readonly Timer _closingTimer;
        private readonly Timer _closeTimer;
        private string _title;
        private string _message;
        private string _severity;

        public event EventHandler<EventArgs> AlertElapsed;
        public DateTimeOffset DateCreated { get; private set; }
        public bool HasElapsed { get; private set; }

        public string Title => _title;
        public string Message => _message;
        public string Severity => _severity;

        public AlertModel(string title, string message, string severity)
        {
            this._title = title;
            this._message = message;
            this._severity = severity;

            DateCreated = DateTimeOffset.Now;

            _closingTimer = new Timer(5000);
            _closingTimer.Start();
            _closingTimer.Elapsed += (sender, args) =>
            {
                HasElapsed = true;
                _closeTimer.Start();

            };

            _closeTimer = new Timer(1000);
            _closeTimer.Elapsed += (sender, args) => Elapsed();
        }

        private void Elapsed()
        {
            HasElapsed = true;
            AlertElapsed?.Invoke(this,EventArgs.Empty);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if(!disposing) return;
            if (_closeTimer == null) return;

            _closeTimer.Dispose();
            _closingTimer.Dispose();
        }
    }
}